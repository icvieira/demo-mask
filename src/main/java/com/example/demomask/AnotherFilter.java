package com.example.demomask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(2)
public class AnotherFilter implements Filter {

    private final Logger log = LoggerFactory.getLogger(AnotherFilter.class);

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        log.info("Starting another filter for request {} {}", req.getMethod(), req.getRequestURI());

        chain.doFilter(request, response);
        log.info("Committing response: {}", res.getContentType());
    }
}
