package com.example.demomask;

import java.util.List;

public class CpfMasker extends Masker {

    public CpfMasker() { }

    public CpfMasker(Masker masker) {
        super(masker);
    }

    @Override
    List<String> getKeys() {
        return List.of("cpf");
    }

    @Override
    String maskValue(String value) {
        return value
                .trim()
                .replaceAll("[.-]", "")
                .substring(0, 3)
                .concat(".***.***-**");
    }
}
