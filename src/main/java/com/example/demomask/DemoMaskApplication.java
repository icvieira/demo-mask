package com.example.demomask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoMaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoMaskApplication.class, args);
	}

}
