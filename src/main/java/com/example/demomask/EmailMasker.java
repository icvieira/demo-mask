package com.example.demomask;

import java.util.List;

public class EmailMasker extends Masker {

    public EmailMasker() { }

    public EmailMasker(Masker masker) {
        super(masker);
    }

    @Override
    List<String> getKeys() {
        return List.of("email");
    }

    @Override
    String maskValue(String value) {
        String[] split = value.split("@");
        String a;
        if (split[0].length() > 3) {
            a = split[0].substring(0, 3).concat("***");
        } else {
            a = split[0].substring(0, 2).concat("***");
        }
        return "%s@%s".formatted(a, split[1]);
    }
}
