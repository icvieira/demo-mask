package com.example.demomask;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static com.jayway.jsonpath.JsonPath.using;

public abstract class Masker {

    private final Logger log = LoggerFactory.getLogger(Masker.class);

    private Masker msk;

    protected Masker() { }

    protected Masker(Masker masker) {
        this.msk = masker;
    }

    abstract List<String> getKeys();

    abstract String maskValue(String value);

    public String mask(String json) {
        if (msk != null) {
            json = msk.mask(json);
        }
        Configuration configuration = Configuration.builder().options(Option.AS_PATH_LIST).build();
        List<String> pathList = new ArrayList<>();
        for (String key : getKeys()) {
            pathList.addAll(using(configuration).parse(json).read("$.." + key));
        }
        log.info("Found {} instances of the keys {}", pathList.size(), getKeys());
        DocumentContext context = JsonPath.parse(json);
        for (String path : pathList) {
            String value = context.read(path);
            context.set(path, maskValue(value));
        }
        return context.jsonString();
    }
 }
