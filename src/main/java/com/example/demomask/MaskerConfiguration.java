package com.example.demomask;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MaskerConfiguration {

    @Bean
    public Masker sensitiveDataMasker() {
        return new CpfMasker(new PhoneMasker(new EmailMasker()));
    }
}
