package com.example.demomask;

import java.util.List;

public class PhoneMasker extends Masker {

    public PhoneMasker() { }

    public PhoneMasker(Masker masker) {
        super(masker);
    }

    @Override
    List<String> getKeys() {
        return List.of("cellphone");
    }

    @Override
    String maskValue(String value) {
        String rawValue = value.trim().replaceAll("[()-]", "");
        String a = rawValue.substring(0, 2);
        String b = rawValue.substring(rawValue.length() - 4);
        return "(%s) *****-%s".formatted(a, b);
    }
}
