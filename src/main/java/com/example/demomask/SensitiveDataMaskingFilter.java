package com.example.demomask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.util.ContentCachingResponseWrapper;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(1)
public class SensitiveDataMaskingFilter implements Filter {

    private final Logger log = LoggerFactory.getLogger(SensitiveDataMaskingFilter.class);

    private final Masker masker;

    public SensitiveDataMaskingFilter(Masker masker) {
        this.masker = masker;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        log.info("Starting sensitive data masking for {} {}", httpServletRequest.getMethod(), httpServletRequest.getRequestURI());

        ContentCachingResponseWrapper wrapper = new ContentCachingResponseWrapper(httpServletResponse);
        chain.doFilter(request, wrapper);

        final boolean mediaTypeIsNotJson = !wrapper.getContentType().contains(MediaType.APPLICATION_JSON_VALUE);
        if (mediaTypeIsNotJson) {
            log.info("Skipping sensitive data masking for {} {}", httpServletRequest.getMethod(), httpServletRequest.getRequestURI());
            wrapper.copyBodyToResponse();
            return;
        }

        String content = new String(wrapper.getContentAsByteArray(), wrapper.getCharacterEncoding());
        wrapper.resetBuffer();
        wrapper.getOutputStream().print(masker.mask(content));
        wrapper.copyBodyToResponse();
        log.info("Committing masking for {} {}", httpServletRequest.getMethod(), httpServletRequest.getRequestURI());
    }
}
