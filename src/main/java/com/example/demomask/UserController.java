package com.example.demomask;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @GetMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> users() {
        return ResponseEntity.ok("""
                {
                    "name": "Igor",
                    "cpf": "730.912.511-87",
                    "cellphone": "(61) 992778368",
                    "users": [
                        {
                            "cpf": "78451727077",
                            "cellphone": "6139639291",
                        },
                        {
                            "cpf": "269.467.340-68",
                            "cellphone": "(61)992779087",
                            "email": "abc@ilia.digital"
                        }
                    ],
                    "email": "igor.vieira@ilia.digital"
                }
                """);
    }
}
